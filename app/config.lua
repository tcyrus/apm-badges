-- config.lua
local config = require('lapis.config')

config('docker', {
  port = os.getenv("PORT")
})
