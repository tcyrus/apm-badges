local lapis = require('lapis')
local http = require('lapis.nginx.http')
local util = require('lapis.util')
local app = lapis.Application()

local function Theme(s, f, mc, sc)
  return {
    stroke = s,
    fill = f,
    main_color = mc,
    sub_color = sc
  }
end

local badge_themes = {
  ['one-light'] = Theme('#E5E5E6', '#FAFAFA', '#424243', '#939395'),
  ['one-dark'] = Theme('#D1D4DC', '#E8EAED', '#2C313A', '#757F95'),
  ['solarized-light'] = Theme('#E4E0D3', '#F4F2EC', '#262217', '#A6976E'),
  ['solarized-dark'] = Theme('#CCDCE0', '#E6EEEF', '#273A3F', '#6796A2'),
}

app:enable('etlua')

app:match('/', function(self)
  return { redirect_to = '/static/' }
end)

app:get('/apm/:name.svg', function(self)
  local name = self.params.name:gsub('[|&;$%@"<>()+,]', '')

  local body, status_code, _ = http.simple('https://atom.io/api/packages/' .. name)

  local json_res = util.from_json(body)

  if json_res['message'] ~= nil then
    print('Package ' .. name .. ' Gives Error ' .. json_res['message'])
    return { status = 500, layout = false, json_res['message'] }
  end

  local badge_theme = badge_themes['one-light'];
  if (self.params['theme'] ~= nil) and (badge_themes[self.params.theme] ~= nil) then
    badge_theme = badge_themes[self.params.theme]
  end

  self.badge_stroke = badge_theme.stroke
  self.badge_fill = badge_theme.fill
  self.badge_mc = badge_theme.main_color
  self.badge_sc = badge_theme.sub_color

  self.badge_name = json_res['name']
  self.badge_downloads = json_res['downloads']
  self.badge_stars = json_res['stargazers_count']
  self.badge_version = json_res['releases']['latest']

  return {
    layout = false,
    render = 'badge',
    content_type = 'image/svg+xml',
    headers = {
      ['Cache-Control'] = 'private,max-age=0,no-cache,no-store',
      ['Pragma'] = 'no-cache'
    }
  }
end)

return app
