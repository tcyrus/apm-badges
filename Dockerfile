FROM openresty/openresty:alpine-fat

RUN apk add openssl-dev

RUN /usr/local/openresty/luajit/bin/luarocks install lapis

ENV LAPIS_OPENRESTY "/usr/local/openresty/bin/openresty"

RUN mkdir /app
WORKDIR /app
ADD ./app /app

ENV PORT "8080"

ENTRYPOINT ["/usr/local/openresty/luajit/bin/lapis"]
CMD ["server", "docker"]
